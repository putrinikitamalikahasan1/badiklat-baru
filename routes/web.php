<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DiklatController;
use App\Http\Controllers\DiklatSurveyController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\MasterWIController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// ---------------LOGIN------------------
Route::get('/', [LoginController::class, 'login'])->name('login');
Route::post('actionlogin', [LoginController::class, 'actionlogin'])->name('actionlogin');
Route::get('home', [HomeController::class, 'index'])->name('home')->middleware('auth');
Route::get('actionlogout', [LoginController::class, 'actionlogout'])->name('actionlogout')->middleware('auth');
// ---------------END---------------------

// ----------USER_MANAGEMENT-------------
Route::get('user_management', [LoginController::class, 'index'])->name('user_management');
Route::get('user_management/delete/{id}', [LoginController::class, 'destroy'])->name('user_management.destroy');
Route::get('user_management/create', [LoginController::class, 'create'])->name('user_management.create');
Route::post('user_management/create/store', [LoginController::class, 'actionregister'])->name('user_management.store');
// ----------END--------------------------


// ------------DIKLAT----------------------
Route::get('diklat', [DiklatController::class, 'index'])->name('diklat');
Route::get('diklat/edit/{id}', [DiklatController::class, 'edit'])->name('diklat.edit');
Route::post('diklat/add', [DiklatController::class, 'store'])->name('diklat.store');
Route::patch('diklat/update/{id}', [DiklatController::class, 'update'])->name('diklat.update');
Route::get('diklat/destroy/{id}', [DiklatController::class, 'destroy'])->name('diklat.destroy');
Route::get('diklat/show/{id}', [DiklatController::class, 'show'])->name('diklat.show');
Route::post('simpanpersyaratan', [DiklatController::class, 'simpanpersyaratan'])->name('simpanpersyaratan');
// ------------END--------------------------


// ------------MASTERPERTANYAAN----------------------
Route::get('master_pertanyaan', [QuestionController::class, 'index'])->name('master_pertanyaan');
Route::get('master_pertanyaan/create', [QuestionController::class, 'create'])->name('master_pertanyaan.create');
Route::post('master_pertanyaan/create/store', [QuestionController::class, 'store'])->name('master_pertanyaan.store');
Route::get('master_pertanyaan/edit/{id}', [QuestionController::class, 'edit'])->name('master_pertanyaan.edit');
Route::patch('master_pertanyaan/update/{id}', [QuestionController::class, 'update'])->name('master_pertanyaan.update');
Route::get('master_pertanyaan/destroy/{id}', [QuestionController::class, 'destroy'])->name('master_pertanyaan.destroy');
Route::get('master_pertanyaan/show/{id}', [QuestionController::class, 'show'])->name('master_pertanyaan.show');
// ------------END------------------------------------


// ------------MASTER_WI-----------------------------
Route::get('master_wi', [MasterWIController::class, 'index'])->name('master_wi');
Route::get('master_wi/create', [MasterWIController::class, 'create'])->name('master_wi.create');
Route::post('master_wi/create/store', [MasterWIController::class, 'store'])->name('master_wi.store');
Route::get('master_wi/edit/{id}', [MasterWIController::class, 'edit'])->name('master_wi.edit');
Route::post('master_wi/update/{id}', [MasterWIController::class, 'update'])->name('master_wi.update');
Route::get('master_wi/destroy/{id}', [MasterWIController::class, 'destroy'])->name('master_wi.destroy');
Route::get('master_wi/show/{id}', [MasterWIController::class, 'show'])->name('master_wi.show');
// ------------END------------------------------------


// ------------DIKLAT_SURVEY-------------------------
Route::get('diklat_survey', [DiklatSurveyController::class, 'index'])->name('diklat_survey');
Route::post('diklat_survey/simpan_survey', [DiklatSurveyController::class, 'store'])->name('simpan_survey.store');
Route::get('response', [DiklatSurveyController::class, 'response'])->name('response');
Route::get('/showSurvey/{nama}', [DiklatSurveyController::class, 'showSurvey'])->name('showSurvey');
Route::get('/response/survey/{nama}', [DiklatSurveyController::class, 'get_survey']);
// ------------END------------------------------------





?>
