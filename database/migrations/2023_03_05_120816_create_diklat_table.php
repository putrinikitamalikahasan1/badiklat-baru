<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiklatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diklat', function (Blueprint $table) {
            $table->id();
            $table->string('judul');
            $table->string('jenis_diklat');
            $table->string('kategori_diklat');
            $table->string('nama_lembaga');
            $table->string('tempat');
            $table->date('tanggal_mulaidaftar');
            $table->date('tanggal_akhirdaftar');
            $table->date('tanggal_mulai');
            $table->date('tanggal_selesai');
            $table->integer('jumlah_peserta');
            $table->string('satuan_durasi');
            $table->string('durasi');
            $table->longText('ketentuan_umum');
            $table->longText('ketentuan_satker');
            $table->longText('ketentuan_peserta');
            $table->json('dokumen');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diklat');
    }
}
?>