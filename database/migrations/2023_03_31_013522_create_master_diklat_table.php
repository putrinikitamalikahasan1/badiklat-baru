<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterDiklatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_diklat', function (Blueprint $table) {
            $table->id();
            $table->string('tahun_ajaran');
            $table->string('data_id');
            $table->string('jenis_diklat');
            $table->string('nama_diklat');
            $table->string('nomor_sertifikat');
            $table->string('tanggal_sertifikat');
            $table->string('tanggal_mulai');
            $table->string('tanggal_selesai');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_diklat');
    }
}
