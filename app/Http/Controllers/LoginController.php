<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Http;
use Validator;
use RealRashid\SweetAlert\Facades\Alert;

class LoginController extends Controller
{
    
    public function login()
    {
        if (Auth::check()) {
            Alert::success('Anda Berhasil Login');
            return redirect('home');
        }else{
            return view('login');
        }
    }

    public function actionlogin(Request $request)
    {
        $data = [
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ];

        if (Auth::Attempt($data)) {
            Alert::success('Anda Berhasil Login');
            return redirect('home');
        }else{
            Alert::warning('Harap Lengkapi email dan password dengan benar');
            return redirect('/');
        }
    }

    public function actionlogout()
    {
        Auth::logout();
        Alert::success('Anda Berhasil Logout');
        return redirect('/login');
    }

    public function create()
    {
        return view('usermanage.create');
    }

    public function index()
    {
        $regist= User::all();
        return view('usermanage.index', compact('regist'));
    }

    public function destroy($id)
    {
        $hapus   = User::find($id);
        $hapus->delete();

        Alert::success('Data Berhasil Terhapus');
        return redirect('user_management');
    }
    
    public function actionregister(Request $request)
    {
        $user = User::create([
            'nama' => $request->nama,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => $request->role,
            'active' => 1
        ]);

        Alert::success('Data Berhasil Ditambahkan');
        return redirect('user_management');
    }
}
?>