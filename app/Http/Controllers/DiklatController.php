<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SyaratDokumen;
use Illuminate\Support\Facades\DB;
use App\Models\Diklat;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;

class DiklatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $diklat = Diklat::all();
        // // $master_syarat_dokumen = SyaratDokumen::all();
        // // dd($diklat);
        // return view('diklat.index', compact('diklat'));
        // $on_page = is_null($request->get('page')) ? 1 : $request->get('page');

        // $res = Http::get('http://103.113.132.182:11091/diklat?cariNamaDiklat=&page=1&size=100'. $on_page);

        // $data['NamaDiklat'] = $res->json()['payload'];
       
        // dd($data);
        // return view('diklat.index', $data);
        // $response = Http::get('http://103.113.132.182:11091/diklat?cariNamaDiklat=&page=1&size=100');

        // $on_page = is_null($request->get('page')) ? 1 : $request->get('page');

        // $res = Http::get('http://103.113.132.182:11091/diklat?cariNamaDiklat=&page'. $on_page);

        // $data['users'] = $res->json()['payload'];
        // // $data['max_pages'] = $res->json()['total_pages'];
        
        // dd($data);
        // return view('diklat.index', compact('data'));
        // $diklat = 'http://103.113.132.182:11091/diklat?cariNamaDiklat=&page';
        // $konten = json_decode(file_get_contents($diklat));
        // dd($konten);
                
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://103.113.132.182:11091/diklat?cariNamaDiklat=&page=1&size=100%0A',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $data = json_decode($response, true);
        $item = $data["payload"]["items"];
        // dd($item);
        return view('diklat.index', compact('item'));
        // echo $response;
         
        // if ($response->toPsrResponse()->getStatusCode() == 200) {
        //     $data = json_decode($response); //bisa  di dd() untuk mengetahui struktur arraynya
        // } else {
        //     $data = []; //atau bisa tampilkan pesan jika data tidak ada
        // }

        // dd($data);

    }
   
    function day_idn($day) {
        $hari = array (
            0 => 'Minggu',
            1 => 'Senin',
            2 => 'Selasa',
            3 => 'Rabu',
            4 => 'Kamis',
            5 => 'Jumat',
            6 => 'Sabtu'
        );
        return $hari[$day];
    }

    function month_idn($month) {
        $bulan = array (
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember'
        );
        return $bulan[$month];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $master_syarat_dokumen = SyaratDokumen::all();
        return view('diklat.create', compact('master_syarat_dokumen'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // $url = 'http://103.113.132.182:11091/diklat?cariNamaDiklat=&page=1&size=100';
        // $Client = new Client([
        //     'form_params' => [
        //         'no_tilang' => $request["nama_diklat"]
        //     ],
        //     'verify' => false
        // ]);
        
        // $response = curl_exec($curl);

        // curl_close($curl);
        // $data = json_decode($response, true);
        // $item = $data["payload"]["items"];
        
        // $response = $Client->post($url);

        // $result = json_decode($response->getBody()->getContents(), true);
        // foreach($result as $result){
        //     $data = [
        //             'id' => $result['id'],
        //             'tahun_ajaran' => $result['tahun_ajaran'],
        //             'jenis_diklat' => $result['jenis_diklat'],
        //             'nama_diklat' => $result['nama_diklat'],
        //             'nomor_sertifikat' => $result['nomor_sertifikat'],
        //             'tanggal_sertifikat' => $result['tanggal_sertifikat'],
        //             'tanggal_mulai' => $result['tanggal_mulai'],
        //             'tanggal_selesai' => $result['tanggal_selesai'],
        //             'status' => $result['status'],
        //             'created_at'        => Carbon::now(),
        //     ];
          
        //     }
        //     dd($data);
              
        // $diklat = Diklat::all();
        // $dokumen = json_encode($request['dokumen']);
        // Diklat::create([
        //     'judul'=> $request['judul'],
        //     'jenis_diklat' => $request['jenis_diklat'],
        //     'kategori_diklat' => $request['kategori_diklat'],
        //     'nama_lembaga' => $request['nama_lembaga'],
        //     'tempat' => $request['tempat_diklat'],
        //     'tanggal_mulaidaftar' => $request['tanggal_mulaidaftar'],
        //     'tanggal_akhirdaftar' => $request['tanggal_akhirdaftar'],
        //     'tanggal_mulai' => $request['tanggal_mulai'],
        //     'tanggal_selesai' => $request['tanggal_selesai'],
        //     'jumlah_peserta' => $request['jumlah_peserta'],
        //     'satuan_durasi' => $request['satuan_durasi'],
        //     'durasi' => $request['durasi'],
        //     'ketentuan_umum' => $request['ketentuan_umum'],
        //     'ketentuan_satker' => $request['ketentuan_satker'],
        //     'ketentuan_peserta' => $request['ketentuan_peserta'],
        //     'dokumen' => $dokumen,
        //     // 'dokumen' => $request['dokumen'],
        //     'status' => $request['status'],   
        //     'created_at'        => Carbon::now(),
        //     'updated_at'        => Carbon::now()
        // ]);

     

        // // $diklat = [
           
        // //         'judul'=> $request['judul'],
        // //         'jenis_diklat' => $request['jenis_diklat'],
        // //         'kategori_diklat' => $request['kategori_diklat'],
        // //         'nama_lembaga' => $request['nama_lembaga'],
        // //         'tempat' => $request['tempat_diklat'],
        // //         'tanggal_mulaidaftar' => $request['tanggal_mulaidaftar'],
        // //         'tanggal_akhirdaftar' => $request['tanggal_akhirdaftar'],
        // //         'tanggal_mulai' => $request['tanggal_mulai'],
        // //         'tanggal_selesai' => $request['tanggal_selesai'],
        // //         'jumlah_peserta' => $request['jumlah_peserta'],
        // //         'satuan_durasi' => $request['satuan_durasi'],
        // //         'durasi' => $request['durasi'],
        // //         'ketentuan_umum' => $request['ketentuan_umum'],
        // //         'ketentuan_satker' => $request['ketentuan_satker'],
        // //         'ketentuan_peserta' => $request['ketentuan_peserta'],
        // //         'dokumen' => $dokumen,
        // //         'status' => $request['status'],
        // // ];
        // // dd($idKunjungan);

        // // dd($data);
       
        
        // // foreach($diklat as $data){
        // //     Diklat::create($data);
        // // }
  
        // // dd($diklat);
        // Alert::success('Data Berhasil Tersimpan');
        // return redirect('diklat');
    }

    function detail($id){

        $res = Http::get('http://103.113.132.182:11091/diklat?cariNamaDiklat=&page=1&size=100'. $id);

        $data['user'] = $res->json()['payload'];

        return view('detail', $data);
    }


    public function simpanpersyaratan(Request $request)
    {
       
     
        SyaratDokumen::create([
            'nama'=> $request['nama_dokumen'],
            'created_at'        => Carbon::now(),
            'updated_at'        => Carbon::now()
        ]);

     

        // $diklat = [
           
        //         'judul'=> $request['judul'],
        //         'jenis_diklat' => $request['jenis_diklat'],
        //         'kategori_diklat' => $request['kategori_diklat'],
        //         'nama_lembaga' => $request['nama_lembaga'],
        //         'tempat' => $request['tempat_diklat'],
        //         'tanggal_mulaidaftar' => $request['tanggal_mulaidaftar'],
        //         'tanggal_akhirdaftar' => $request['tanggal_akhirdaftar'],
        //         'tanggal_mulai' => $request['tanggal_mulai'],
        //         'tanggal_selesai' => $request['tanggal_selesai'],
        //         'jumlah_peserta' => $request['jumlah_peserta'],
        //         'satuan_durasi' => $request['satuan_durasi'],
        //         'durasi' => $request['durasi'],
        //         'ketentuan_umum' => $request['ketentuan_umum'],
        //         'ketentuan_satker' => $request['ketentuan_satker'],
        //         'ketentuan_peserta' => $request['ketentuan_peserta'],
        //         'dokumen' => $dokumen,
        //         'status' => $request['status'],
        // ];
        // dd($idKunjungan);

        // dd($data);
       
        
        // foreach($diklat as $data){
        //     Diklat::create($data);
        // }
  
        // dd($diklat);
        Alert::success('Data Berhasil Tersimpan, Silahkan isi kembali form');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        // dd($id);
        $check_diklat = Diklat::where('data_id', $id)->count();
        if($check_diklat >= 1){
            Alert::error('Data Telah Disimpan Sebelumnya!');
            return redirect('diklat');
        }else{
            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://103.113.132.182:11091/diklat?cariNamaDiklat=&page=1&size=100%0A',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            )); 
    
            $response = curl_exec($curl);
    
            // $master_wi = masterWI::where('id', $id)->update;
    
            curl_close($curl);
            $data = json_decode($response, true);
            $item = $data["payload"]["items"];
            // $data = $item[$id];
            // $data = $item.find($id);
            $filteredArray = Arr::where($item, function ($value, $key) use($id) {
                return $value['id'] == $id;
            });
            // dd($filteredArray);
            $key = array_search($id, array_column($item, 'id'));        
            // dd($key);
    
            $object = json_decode(json_encode($filteredArray[$key]), TRUE);
            $tahun_ajaran = json_encode($object['tahun_ajaran']);
            // dd($object);
    
            Diklat::create([
                'data_id'=> $object['id'],
                'tahun_ajaran'=> $tahun_ajaran,
                'jenis_diklat'=> $object['jenis_diklat'],
                'nama_diklat'=>$object['nama_diklat'],
                'nomor_sertifikat'=>$object['nomor_sertifikat'],
                'tanggal_sertifikat'=>$object['tanggal_sertifikat'],
                'tanggal_mulai'=>$object['tanggal_mulai'],
                'tanggal_selesai'=>$object['tanggal_selesai'],
                'status'=>$object['status'],
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now()
            ]);
    
            // Diklat::create($object);
            Alert::success('Data Berhasil Tersimpan!');
            return  redirect('diklat');
    
        }
       
    //     $url = 'http://103.113.132.182:11091/diklat?cariNamaDiklat=&page=1&size=100%0A';
    //     $Client = new Client([
    //         'form_params' => [
    //             'id' => $id["id"]
    //         ],
    //         'verify' => false
    //     ]);

    //     $response = $Client->post($url);

        
    //     $result = json_decode($response->getBody()->getContents(), true);


    //     foreach($result as $result){
        
    //    $kunjungan=
    //         [
    //             'id' => $result['id'],
    //             'nama_diklat' => $result['nama_diklat'],
    //             'jenis_diklat' => $result['jenis_diklat'],
               
    //             ]
    //         ;
        }


        
        // return view('diklat.index', compact('item'));

        // $diklat = Diklat::find($id);
        // $id = ($diklat->id);
        // $master_syarat_dokumen = SyaratDokumen::all();

        // return view("diklat.edit", compact('diklat','master_syarat_dokumen'));
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dokumen = json_encode($request['dokumen']);

        $dataDiklat = Diklat::where('id', $id)->update([
            'judul'=> $request['judul'],
            'jenis_diklat' => $request['jenis_diklat'],
            'kategori_diklat' => $request['kategori_diklat'],
            'nama_lembaga' => $request['nama_lembaga'],
            'tempat' => $request['tempat_diklat'],
            'tanggal_mulaidaftar' => $request['tanggal_mulaidaftar'],
            'tanggal_akhirdaftar' => $request['tanggal_akhirdaftar'],
            'tanggal_mulai' => $request['tanggal_mulai'],
            'tanggal_selesai' => $request['tanggal_selesai'],
            'jumlah_peserta' => $request['jumlah_peserta'],
            'satuan_durasi' => $request['satuan_durasi'],
            'durasi' => $request['durasi'],
            'ketentuan_umum' => $request['ketentuan_umum'],
            'ketentuan_satker' => $request['ketentuan_satker'],
            'ketentuan_peserta' => $request['ketentuan_peserta'],
            'dokumen' => $dokumen,
            // 'dokumen' => $request['dokumen'],
            'status' => $request['status'],   
            'updated_at'        => Carbon::now()
        ]);
        

        Alert::success('Data Berhasil Di ubah');
        return redirect('diklat');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $diklat   = Diklat::find($id);
        $diklat->delete();

        Alert::success('Data Berhasil Terhapus');
        return redirect('diklat');
    }

    
}
?>