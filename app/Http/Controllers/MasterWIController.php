<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\masterWI;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Carbon;

class MasterWIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $masterWI = masterWI::all();
        // $master_syarat_dokumen = SyaratDokumen::all();
        // dd($diklat);
        return view('master_wi.index', compact('masterWI'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master_wi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        masterWI::create([
            'status'=> $request['status'],
            'nama'=> $request['nama'],
            'jenis_kelamin'=> $request['jk'],
            'tanggal_lahir'=> $request['tgl_lahir'],
            'instansi'=> $request['instansi'],
            'jabatan'=> $request['jabatan'],
            'created_at'        => Carbon::now(),
            'updated_at'        => Carbon::now()
        ]);

        Alert::success('Data Berhasil Tersimpan!');
        return  redirect('master_wi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $master_wi = masterWI::find($id);
        return view("master_wi.detail", compact('master_wi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $master_wi = masterWI::find($id);
        $id = ($master_wi->id);
        
        return view("master_wi.edit", compact('master_wi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $master_wi = masterWI::where('id', $id)->update([
            'status'=> $request['status'],
            'nama'=> $request['nama'],
            'jenis_kelamin'=> $request['jk'],
            'tanggal_lahir'=> $request['tgl_lahir'],
            'instansi'=> $request['instansi'],
            'jabatan'=> $request['jabatan'],
            'updated_at'        => Carbon::now()
        ]);
        

        Alert::success('Data Berhasil Di ubah');
        return redirect('master_wi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $master_wi   = masterWI::find($id);
        $master_wi->delete();

        Alert::success('Data Berhasil Terhapus');
        return redirect('master_wi');
    }
}
