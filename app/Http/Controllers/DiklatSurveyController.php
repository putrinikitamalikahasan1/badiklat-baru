<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pertanyaan;
use App\Models\masterWI;
use App\Models\QuestionMasterr;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class DiklatSurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $pilihan_ganda = QuestionMasterr::select('pertanyaan','jawaban_a','jawaban_b','jawaban_c','jawaban_d','jawaban_e')
        // ->where('is_essai', null)
        // ->orderByRaw('id DESC')
        // ->get();

        // $pilihan_ganda = QuestionMasterr::where('is_essai', '!=',  '1');
        // ->get();

        // $pilihan_ganda = QuestionMasterr::all();
        $pilihan_ganda = QuestionMasterr::where('is_essai', '=', null)->get();

        $essai = QuestionMasterr::select('*')
        ->where('is_essai', !null)
        ->orderByRaw('id DESC')
        ->get();
         
        // $pilihan_ganda['essai'] = QuestionMasterr::select('*')
        // ->where('is_essai', !null)
        // ->orderByRaw('id DESC')
        // ->get();
         
        // $pilihan_ganda->push($essai);
        // $essai = QuestionMasterr::select('*')
        // ->orderByRaw('is_essai ASC')
        // ->get();

        $pertanyaan = QuestionMasterr::all();
        $masterWI = masterWI::all();
        // // $master_syarat_dokumen = SyaratDokumen::all();
        // dd($pilihan_ganda);
        return view('diklat_survey.index', compact('pilihan_ganda','essai','pertanyaan','masterWI'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $jawaban = json_encode($request['jawaban']);
        // $pertanyaan = json_encode($request['pertanyaan']);
        // Pertanyaan::create([
        //     'nama'=> $request['nama'],
        //     'created_at'        => Carbon::now(),
        //     'updated_at'        => Carbon::now()
        // ]);
        // $data = $request->map(function)
        // $data = [
        //         'nama'=> $request['nama'],
        //         'pertanyaan' => $pertanyaan,
        //         'jawaban' => $jawaban,
        // ];
        // $request->validate($request, [
        //     'master_wi_id' => 'required',
        //     'pertanyaan' => 'required',
        //     'jawaban' => 'required'
        // ],[
        //     'master_wi_id.required' => "Nama WI tidak boleh kosong"
        // ]);

        $data = $request->id;

        foreach ($data as $key => $value) {
            # code...
            $pertanyaan = QuestionMasterr::find($value);
            $masterWI = masterWI::all();
            Pertanyaan::create([
            'nama'=> $request->nama, 
            'pertanyaan'=> $pertanyaan->pertanyaan,
            'jawaban'=> $request[$value],
            'created_at'        => Carbon::now(),
            'updated_at'        => Carbon::now()
        ]);
       
        }
        Alert::success('Terimakasih telah melengkapi pertanyaan!');
        return back();
    }

    public function response()
    {
        // $response =  DB::table('data_pertanyaan as p')
        // ->join('master_wi as m','p.nama','=','m.id')
        // ->select('p.nama', 'p.created_at', 'm.nama')
        // ->groupBy('p.nama','p.created_at')
        // ->get();

        $masterWI = masterWI::all();
        $response = Pertanyaan::select('nama', 'created_at')
        ->groupBy('nama','created_at')        
        ->get();

        // $master_syarat_dokumen = SyaratDokumen::all();
        // dd($response);
        return view('diklat_survey.response', compact('response', 'masterWI'));
    }

    public function showSurvey($nama){
        $response = Pertanyaan::where('nama', $nama)->get();
        return view('diklat_survey.modalSurvey', compact('response'));
    } 

    public function get_survey($nama)
    {
        
     
        // $data_survey = DB::table('survey_response')
        // ->select('*')
        // ->where('kunjungan_id', $kunjungan_id)
        // ->get();
        $data_survey = Pertanyaan::where('nama', $nama)->get();


        
        return $data_survey;
    }


  
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
