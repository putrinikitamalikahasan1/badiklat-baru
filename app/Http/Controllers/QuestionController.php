<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\QuestionMasterr;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Carbon;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $question = QuestionMasterr::all();
        
        // $master_syarat_dokumen = SyaratDokumen::all();
        // dd($diklat);
        return view('master_pertanyaan.index', compact('question'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master_pertanyaan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        QuestionMasterr::create([
            'pertanyaan'=> $request['pertanyaan'],
            'jawaban_a'=> $request['jawaban_a'],
            'jawaban_b'=> $request['jawaban_b'],
            'jawaban_c'=> $request['jawaban_c'],
            'jawaban_d'=> $request['jawaban_d'],
            'jawaban_e'=> $request['jawaban_e'],
            'is_essai'=> $request['jenis_pertanyaan'],
            'created_at'        => Carbon::now(),
            'updated_at'        => Carbon::now()
        ]);

        Alert::success('Data Berhasil Tersimpan');
        return redirect('master_pertanyaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question = QuestionMasterr::find($id);
        return view("master_pertanyaan.detail", compact('question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $master_pertanyaan = QuestionMasterr::find($id);
        $id = ($master_pertanyaan->id);


        return view("master_pertanyaan.edit", compact('master_pertanyaan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tipe_pertanyaan = $request["jenis_pertanyaan"];
        
        if($tipe_pertanyaan  == '1'){
            QuestionMasterr::where('id', $id)->update([
                'pertanyaan' => $request['pertanyaan'],   
                'pertanyaan' => $request['pertanyaan'], 
                'jawaban_a' => null,   
                'jawaban_b' => null,   
                'jawaban_c' => null,   
                'jawaban_d' => null,   
                'jawaban_e' => null, 
                'jawaban_e' => null,   
                'is_essai' => $request['jenis_pertanyaan'],   
                'updated_at'        => Carbon::now()
            ]);
        }else if($tipe_pertanyaan  == null){
            // $jawaban_a = $request["jawaban_a"];
            // $jawaban_b = $request["jawaban_b"];
            // $jawaban_c = $request["jawaban_c"];
            // $jawaban_d = $request["jawaban_d"];
            // $jawaban_e = $request["jawaban_e"];

            QuestionMasterr::where('id', $id)->update([
                'pertanyaan' => $request['pertanyaan'], 
                'pertanyaan' => $request['pertanyaan'],   
                'jawaban_a' => $request['jawaban_a'],   
                'jawaban_b' => $request['jawaban_b'],   
                'jawaban_c' => $request['jawaban_c'],   
                'jawaban_d' => $request['jawaban_d'],   
                'jawaban_e' => $request['jawaban_e'],  
                'jawaban_e' => $request['jawaban_e'], 
                'is_essai' => $request['jenis_pertanyaan'],   
                'updated_at'        => Carbon::now()
            ]);
                
        }else{
            echo "data tidak diketahui";
        }

        // $dataQuestion = QuestionMasterr::where('id', $id)->update([
        //     'pertanyaan' => $request['pertanyaan'],   
        //     'jawaban_a' => $request['jawaban_a'],   
        //     'jawaban_b' => $request['jawaban_b'],   
        //     'jawaban_c' => $request['jawaban_c'],   
        //     'jawaban_d' => $request['jawaban_d'],   
        //     'jawaban_e' => $request['jawaban_e'],   
        //     'is_essai' => $request['jenis_pertanyaan'],   
        //     'updated_at'        => Carbon::now()
        // ]);
        

        Alert::success('Data Berhasil Di ubah');
        return redirect('master_pertanyaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $master_pertanyaan   = QuestionMasterr::find($id);
        $master_pertanyaan->delete();

        Alert::success('Data Berhasil Terhapus');
        return redirect('master_pertanyaan');
    }
}
