<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class masterWI extends Model
{
    use HasFactory;

    protected $table = 'master_wi';


    protected $fillable = [
        'id',
        'status',
        'nama',
        'jenis_kelamin',
        'tanggal_lahir',
        'instansi',
        'jabatan',
        'created_at',
        'updated_at',
    ];

    

    public function Pertanyaan()
    {

        return $this->hasMany(Pertanyaan::class);
    }
    

    
}
