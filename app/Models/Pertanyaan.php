<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    use HasFactory;

    protected $table = 'data_pertanyaan';


    protected $fillable = [
        'id',
        'nama',
        'pertanyaan',
        'jawaban',
        'created_at',
        'updated_at'
    ];

    public function masterWI(){
        return $this->hasOne(masterWI::class, 'id','nama');
    }
}
