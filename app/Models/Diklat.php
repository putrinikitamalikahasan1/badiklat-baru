<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Diklat extends Model
{
    use HasFactory;
    protected $table = 'master_diklat';


    protected $fillable = [
        'id',
        'data_id',
        'tahun_ajaran',
        'jenis_diklat',
        'nama_diklat',
        'nomor_sertifikat',
        'tanggal_sertifikat',
        'tanggal_mulai',
        'tanggal_selesai',
        'status',
        'created_at',
        'updated_at'
    ];
}
?>