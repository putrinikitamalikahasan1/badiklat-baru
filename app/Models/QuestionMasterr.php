<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionMasterr extends Model
{
    use HasFactory;
    protected $table = 'master_question';


    protected $fillable = [
        'id',
        'pertanyaan',
        'jawaban_a',
        'jawaban_b',
        'jawaban_c',
        'jawaban_d',
        'jawaban_e',
        'is_essai',
        'created_at',
        'updated_at'
    ];
}
