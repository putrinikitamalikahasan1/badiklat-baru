@extends('layout.admin')
@section('content')
<div class="container-fluid">
    <div class="card mb-4">
        <div class="card-body">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h4 class="m-0 font-weight-bold text-primary">Edit User</h4>
            </div>
            
            <hr>
            <form action="{{route('editregist')}}" method="post">
            @csrf
                <div class="form-group">
                    <label><i class="fa fa-user"></i> Nama</label>
                    <input type="text" name="nama" class="form-control" placeholder="Nama" required="">
                </div>
                <div class="form-group">
                    <label><i class="fa fa-envelope"></i> Email</label>
                    <input type="email" name="email" class="form-control" placeholder="Email" required="">
                </div>
                <div class="form-group">
                    <label><i class="fa fa-key"></i> Password</label>
                    <input type="password" name="password" class="form-control" placeholder="Password" required="">
                </div>
                <div class="form-group">
                    <label><i class="fa fa-address-book"></i> Role</label>
                    <select name="role" class="form-control select2" required>
                                <option value="role user tidak diketahui">==== Pilih Tujuan ====</option>
                                <option value="administrator">Administrator</option>
                                <option value="user">User</option>
                    </select>
                </div>
                <a href = "{{ url('/registerindex') }}" type="button" id="loads" class="btn btn-base btn-danger" style=" "><i class="fa fa-arrow-left me-3"></i>KEMBALI</a>
                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-user"></i> Ubah Data</button>
             
                   
            </form>                 
        </div>
    </div>
</div>

@endsection