@extends('layout.admin')
@section('content')
<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h3 class="m-0 font-weight-bold text-primary">User Management</h3>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-download fa-sm text-white-50"></i> Unduh Data
            </a>
    </div>
    <div class="card mb-4">
        <div class="card-body">
        @include('sweetalert::alert')
            <div class="white-box p-1">
                <a href="{{ route('user_management.create') }}" class="btn btn-primary"><i class="fas fa-user-plus"></i>  Tambah Data</a><br>
            </div>
            <div class="table-responsive">
				<table id="" class="table table-striped p-1" cellspacing="0" width="100%" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php $no = 1 ?>
                            @foreach($regist as $data)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{$data->nama}}</td> 
                                <td>{{$data->email}}</td> 
                                <td>{{$data->role}}</td> 
                                <td><?php if ($data->active==1){
                                    echo "Aktif";
                                    }
                                    else{
                                    echo ("Tidak Aktif");
                                    }?>
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                           Action
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a href="{{ route('user_management.destroy', $data->id) }}" class="dropdown-item">Hapus</a>                                      
                                    </div>
                                </td>  
                            </tr>
                            @endforeach
                        </tr>                              
                    </tbody>
                </table>      
            </div>
        </div>
    </div>
</div>

@endsection