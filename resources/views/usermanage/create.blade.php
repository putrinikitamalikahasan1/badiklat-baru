@extends('layout.admin')
@section('content')
<div class="container-fluid">
    <div class="card mb-4">
        <div class="card-body">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h4 class="m-0 font-weight-bold text-primary">Tambah User</h4>
            </div>

            <hr>
            <form action="{{route('user_management.store')}}" method="post">
            @csrf
                <div class="form-group">
                    <label> Nama</label>
                    <label style="color: red;">*</label>
                    <input type="text" name="nama" class="form-control" placeholder="Nama" required="">
                </div>
                <div class="form-group">
                    <label> Email</label>
                    <label style="color: red;">*</label>
                    <input type="email" name="email" class="form-control" placeholder="Email" required="">
                </div>
                <div class="form-group">
                    <label> Password</label>
                    <label style="color: red;">*</label>
                    <input type="password" name="password" class="form-control" placeholder="Password" required="">
                </div>
                <div class="form-group">
                    <label> Role</label>
                    <label style="color: red;">*</label>
                    <select name="role" class="form-control select2" required>
                                <option value="role user tidak diketahui">==== Pilih Tujuan ====</option>
                                <option value="administrator">Administrator</option>
                                <option value="user">User</option>
                              </select>
                </div>
                <div class="modal-footer">   
                    <a href = "{{ url('/user_management') }}" type="button"class="btn btn-base btn-danger"><i class="fa fa-arrow-left ms-3"></i>  KEMBALI</a>
                    <button type="submit" class="btn btn-base btn-success">SIMPAN <i class="fa fa-folder ms-3"></i></button>
                </div>
            </form>                 
        </div>
    </div>
</div>
  
@endsection