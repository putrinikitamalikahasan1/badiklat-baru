@extends('layout.admin')
@section('content')
<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h3 class="m-0 font-weight-bold text-primary">Master WI</h3>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-download fa-sm text-white-50"></i> Unduh Data
            </a>
    </div>
    <div class="card mb-4">
        <div class="card-body">  
        @include('sweetalert::alert')         
            <div class="white-box p-1">
                <a href="{{ route('master_wi.create') }}" class="btn btn-primary"><i class="fas fa-user-plus"></i>  Tambah Data</a><br>
            </div>
            <div class="table-responsive">
				<table id="" class="table table-striped p-1" cellspacing="0" width="100%" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Status</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Tanggal Lahir</th>
                            <th>Instansi</th>
                            <th>Jabatan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>              
                    <tbody>
                        <tr>
                            <?php $no = 1 ?>
                            @foreach($masterWI as $data)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{$data->status}}</td> 
                                <td>{{$data->nama}}</td> 
                                <td>{{$data->jenis_kelamin}}</td> 
                                <td>{{$data->tanggal_lahir}}</td> 
                                <td>{{$data->instansi}}</td> 
                                <td>{{$data->jabatan}}</td> 
                                <td>
                                    <div class="dropdown">
                                        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                           Action
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="{{ route('master_wi.edit', $data->id) }}">Edit</a>
                                            <a href="{{ route('master_wi.destroy', $data->id) }}" class="dropdown-item">Hapus</a>
                                            <a href="{{ route('master_wi.show', $data->id) }}" class="dropdown-item">View</a>                                                                            
                                        </div>
                                    </div>
                                </td>  
                            </tr>
                            @endforeach
                        </tr>                              
                    </tbody>
                </table> 
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    function tujuan(that) {
        if (that.value == "") {
            document.getElementById("view_jawaban").style.display = "block";
        } else if (that.value == "1") {  
            document.getElementById("view_jawaban").style.display = "none";
        } 
  }
</script>
@endsection
