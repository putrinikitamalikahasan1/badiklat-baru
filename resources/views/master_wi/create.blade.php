@extends('layout.admin')
@section('content')
<div class="container-fluid">
    <div class="card mb-4">
        <div class="card-body">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h4 class="m-0 font-weight-bold text-primary">Tambah Master WI</h4>
            </div>
            <form action="{{route('master_wi.store')}}" method="POST">
                @csrf
                <div class="form-group">
                        <span>Status</span>
                        <label style="color: red;">*</label>
                        <select name="status" class="form-control select2" required>
                        <option value="pilihan">Pilih Status</option>
                        <option value="ASN">ASN</option>
                        <option value="Non ASN">Non ASN</option>
                        </select>
                </div>
                <div class="form-group">
                     <span>Nama</span>
                     <label style="color: red;">*</label>
                     <input type="text" class="form-control" id="validationCustom01"  name="nama" placeholder="Masukkan Nama Anda...">
                </div>
                <div class="form-group">
                        <span>Jenis Kelamin</span>
                        <label style="color: red;">*</label>
                        <select name="jk" class="form-control select2" required>
                        <option value="pilihan">Pilih Jenis Kelamin</option>
                        <option value="P">Perempuan</option>
                        <option value="L">Laki-Laki</option>
                        </select>
                </div>
                <div class="form-group">
                     <span>Tanggal Lahir</span>
                     <label style="color: red;">*</label>
                     <input type="date" class="form-control" id="validationCustom01"  name="tgl_lahir" >
                </div>
                <div class="form-group">
                     <span>Instansi</span>
                     <label style="color: red;">*</label>
                     <input type="text" class="form-control" id="validationCustom01"  name="instansi" placeholder="Masukkan Nama Instansi...">
                </div>
                <div class="form-group">
                     <span>Jabatan</span>
                     <label style="color: red;">*</label>
                     <input type="text" class="form-control" id="validationCustom01"  name="jabatan" placeholder="Masukkan Jabatan...">
                </div>
                <div class="modal-footer">
                <a href = "{{ url('/master_wi') }}" type="button" id="loads" class="btn btn-base btn-danger" style=" "><i class="fa fa-arrow-left me-3"></i>  KEMBALI</a>
                <button type="submit" class="btn btn-base btn-success">SIMPAN <i class="fa fa-folder ms-3"></i></button>
                </div>
            </form>
        </div>
        </div>
    </div>                 
</div>

@endsection