@extends('layout.admin')
@section('content')
<!-- CONTENT -->
<style type="text/css">
	  .loader {
    border: 8px solid #f3f3f3;
    border-radius: 50%;
    border-top: 8px solid #3498db;
    width: 50px;
    height: 50px;
    -webkit-animation: spin 2s linear infinite; /* Safari */
    animation: spin 2s linear infinite;
    }
              
    /* Safari */
    @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
    }
  
    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }

</style>
<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h3 class="m-0 font-weight-bold text-primary">Response</h3>
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-download fa-sm text-white-50"></i> Unduh Response
        </a>
    </div>
    <div class="card mb-4">
        <div class="card-body">
        @include('sweetalert::alert')
            <div class="table-responsive">
				<table id="" class="table table-striped p-1" cellspacing="0" width="100%" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>  
                            <th>Tanggal Survey</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>          
                    <tbody>
                        <tr>
                            <?php $no = 1 ?>
                            @foreach($response as $data)
                            <tr>
                                <td>{{ $no++ }}</td>
								<td>{{$data->masterWI->nama}}</td>
                                <td>{{$data->created_at}}</td> 
                                <td>
                                <a href="{{ route('showSurvey', $data->nama) }}" data-id="{{$data->nama}}" class="btn btn-primary view-survey">
                                  Hasil Response
                                </a>    
                                </td>  
                            </tr>
                            @endforeach
                        </tr>                              
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="loading" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
		<div class="modal-dialog" role="document">
		<center><div class="loader"></div></center>

		</div>
	</div>
<div class="modal fade" id="view-survey" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
		<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Data Survey</h5>
			<button type="button" class="close" data-dismiss="modal" id="closeModal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>

			</div>
			<div class="modal-body" id="modal-body">
				<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
					<thead>
						<tr>
							<th width="70%">Pertanyaan</th>
							<th width="30%">Jawaban</th>
						</tr>
					</thead>
						
					<tbody id="data-survey">
					</tbody>
					
				</table>
			</div>
			<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal" id="tutup">Tutup</button>
			</div>
		</div>
		</div>
	</div>
<!-- end modal -->
<!-- END CONTENT -->
@endsection
 
<script type="text/javascript"> 
$('#closeModal').on("click", function(event){
	$("#view-survey").modal('hide');

})
$('#tutup').on("click", function(event){
	$("#view-survey").modal('hide');

})
$('.view-survey').on("click", function(event) {
	// console.log('tes');
	// document.getElementById("loading");
	// const loading = document.getElementById("loading");
	// loading.show()
	$('.loader').show();
	const modal = new bootstrap.Modal($('#view-survey'))
	const loading = new bootstrap.Modal($('#loading'))
	$("#loading").modal('show');

    var survey_name = $(this).data('nama');

	const xhttp = new XMLHttpRequest();
	xhttp.open("GET", "survey/" + survey_name);
	xhttp.send();
	xhttp.onreadystatechange = function() {
		if (xhttp.status == 200) {
			var trHTML = ''; 
			
			const objects =JSON.parse(xhttp.responseText);
			if(objects.length !== 0){

				console.log('ini object', objects)
				for (let object of objects) {
					trHTML += '<tr>';
					trHTML += '<td>'+object['pertanyaan']+'</td>';
					trHTML += '<td>'+object['jawaban']+'</td>';
					trHTML += "</tr>";
				}
				// for(let i = 0; i < objects.length; i++){
					// 	console.log(objects[i]);
					// }
					// console.log(objects)
					document.getElementById("data-survey").innerHTML = trHTML;
					$("#view-survey").modal('show');
					$("#loading").modal('hide');


			} else {
				Swal.fire({
				type: 'error',
				title: '<i>Survey Tidak dilakukan oleh tamu ini pada saat kunjungan</i>'

			})
			$("#loading").modal('hide');

			}
		}else {
			Swal.fire({
				type: 'error',
				title: '<i>Survey Tidak dilakukan oleh tamu ini pada saat kunjungan</i>'
			})
			$("#loading").modal('hide');

		}
	}
});
</script>