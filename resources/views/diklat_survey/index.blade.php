@extends('layout.admin')
@section('content')
<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h3 class="m-0 font-weight-bold text-primary">Pertanyaan</h3>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-download fa-sm text-white-50"></i> Unduh Pertanyaan
            </a>
    </div>
    <div class="card mb-4">
        <div class="card-body">
            @include('sweetalert::alert')
                <form action="{{ route('simpan_survey.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="position-option">Nama WI</label>
                        <select class="form-control @error('nama') is-invalid @enderror" id="position-option" name="nama" >
                        <option disable value="">- pilih -</option>
                        @foreach ($masterWI as $position)
                            <option value="{{ $position->id }}">{{ $position->nama }}</option>
                        @endforeach
                        </select>
                        @error('nama')
                        <div class="invalid-feedback">{{$message}}</div>
                        @enderror
                    </div>
                        @foreach ($pilihan_ganda as $data)
                        <span>Pilihan Ganda</span>
                        <hr>   
                        <div class="form-group">
                            <input type="hidden" name="id[]" value="{{$data->id}}">
                            <label name="{{$data->id}}" for="">{{$data->pertanyaan}}</label>
                            <ul class="max-w-md space-y-4 text-black font-Poppins list-disc list-inside @error('id[]') is-invalid @enderror" >
                                <li>
                                    A.<input type="radio" name="{{ $data->id }}" value="{{ $data->jawaban_a }}">  {{ $data->jawaban_a }}
                                </li>
                                <li>
                                    B.<input type="radio" name="{{ $data->id }}" value="{{ $data->jawaban_b }}">  {{ $data->jawaban_b }}
                                </li>
                                <li>
                                    C.<input type="radio" name="{{ $data->id }}" value="{{ $data->jawaban_c }}">  {{ $data->jawaban_c }} 
                                </li>
                                <li>
                                    D.<input type="radio" name="{{ $data->id }}" value="{{ $data->jawaban_d }}">  {{ $data->jawaban_d }}
                                </li>
                                <li>
                                    E.<input type="radio" name="{{ $data->id }}" value="{{ $data->jawaban_e }}">  {{ $data->jawaban_e }}
                                </li>
                            </ul>
                        </div>
                        @endforeach

                    <!-- Page Heading -->
                        @foreach ($essai as $essai)
                        <span>Essai</span>
                        <hr>
                        <input type="hidden" name="id[]" value="{{$essai->id}}">
                            <div class="form-group @error('id[]') is-invalid @enderror">
                                <span>{{ $essai->pertanyaan }}</span>
                                <label style="color: red;">*</label>
                                <textarea class="form-control" rows="10" cols="50" name="{{ $essai->id }}"></textarea>
                            </div>
                        @endforeach
                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-success">SIMPAN <i class="fa fa-folder ms-3"></i></button>
                        </div>
                </form>
        </div>
    </div>
</div>

@endsection