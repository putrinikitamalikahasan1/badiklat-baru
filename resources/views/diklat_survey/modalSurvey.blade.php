@extends('layout.admin')
@section('content')
<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h3 class="m-0 font-weight-bold text-primary">Response Diklat</h3>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-download fa-sm text-white-50"></i> Unduh Pertanyaan
            </a>
    </div>
    <div class="card mb-4">
        <div class="card-body">
            <div class="header" id="header">
                <center><h1>Data Survey</h1></center><br>
            </div>
            <div class="table-responsive">
            <table id="" class="table table-striped p-1" cellspacing="0" width="100%" style="width:100%">
                    <thead>
                        <tr>
                            <th>Pertanyaan</th>
                            <th>Jawaban</th>
                        </tr>
                    </thead>          
                    <tbody>
                        <tr>
                        @foreach($response as $data)
                    <tbody>
                        <tr>
                            <td>{{$data->pertanyaan}}</td>
                            <td>{{$data->jawaban}}</td>
                        </tr>
                    </tbody>
                    @endforeach
                           
                        </tr>                              
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
 
 