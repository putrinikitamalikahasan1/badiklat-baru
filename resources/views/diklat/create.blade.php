@extends('layout.admin')
@section('content')
<!-- CONTENT -->
<div class="container-fluid">
    <div class="card mb-4">
        <div class="card-body">
            @include('sweetalert::alert')
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h4 class="m-0 font-weight-bold text-primary">Form Tambah Diklat</h4>
            </div>
            <form action="{{route('diklat.store')}}" method="POST">
                @csrf
                <hr>
                <div class="form-group">
                    <span>Judul Penyelenggaraan Diklat</span>
                    <label style="color: red;">*</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Judul..." name="judul" required>
                </div>
                <div class="form-group">
                    <span>Jenis Diklat</span>
                    <label style="color: red;">*</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="jenis_diklat" required>
                        <option disable value="">===Pilih===</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                    </select>
                </div>
                <div class="form-group">
                    <span>Kategori Diklat</span>
                    <label style="color: red;">*</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="kategori_diklat" required>
                        <option disable value="">===Pilih===</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                    </select>
                </div>
                <div class="form-group">
                    <span>Nama lembaga yang menyelenggarakan</span>
                    <label style="color: red;">*</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Nama Lembaga..." name="nama_lembaga" required>
                </div>
                <div class="form-group">
                    <span>Tempat Diklat</span>
                    <label style="color: red;">*</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="tempat_diklat" required>
                        <option disable value="">===Pilih===</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                    </select>
                </div>
                <div  class="row g-2 needs-validation">
                    <div class="col-md-6">
                        <span>Tanggal Mulai Pendaftaran Diklat</span>
                        <label style="color: red;">*</label>
                        <input type="date" class="form-control" id="validationCustom01"  name="tanggal_mulaidaftar" required>
                    </div>
                    <div class="col-md-6">
                        <span>Tanggal Akhir Pendaftaran Diklat</span>
                        <label style="color: red;">*</label>
                        <input type="date" class="form-control" id="validationCustom01"  name="tanggal_akhirdaftar" required>
                    </div>
                </div><br>
                <div  class="row g-2 needs-validation">
                    <div class="col-md-6">
                        <span>Tanggal Mulai Diklat</span>
                        <label style="color: red;">*</label>
                        <input type="date" class="form-control" id="validationCustom01"  name="tanggal_mulai" required>
                    </div>
                    <div class="col-md-6">
                        <span>Tanggal Selesai Diklat</span>
                        <label style="color: red;">*</label>
                        <input type="date" class="form-control" id="validationCustom01"  name="tanggal_selesai" required>
                    </div>
                </div><br>
                <div class="form-group">
                    <span>Jumlah Peserta (Orang)</span>
                    <label style="color: red;">*</label>
                    <input type="number" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Jumlah Peserta..." name="jumlah_peserta" required>
                </div>
                <div  class="row g-2 needs-validation">
                    <div class="col-md-6">
                        <span>Durasi</span>
                        <label style="color: red;">*</label>
                        <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Durasi.." name="durasi" required>
                    </div>
                    <div class="col-md-6">
                        <span>Satuan Durasi</span>
                        <label style="color: red;">*</label>
                        <select class="form-control" id="exampleFormControlSelect1" name="satuan_durasi" required>
                            <option value="menit">Menit</option>
                            <option value="jam">Jam</option>
                            <option value="detik">detik</option>
                        </select>
                    </div>
                </div><br>
                <div class="form-group">
                    <span>Ketentuan Umum Mengenai Diklat Ini</span>
                    <label style="color: red;">*</label>
                    <textarea id="ket_umum" class="form-control" rows="10" cols="50" name="ketentuan_umum" ></textarea>
                </div>
                <div class="form-group">
                    <span>Satuan Kerja Yang Ditunjuk Untuk Mengikuti Diklat Ini</span>
                    <label style="color: red;">*</label>
                    <textarea id="ket_satker" class="form-control" rows="10" cols="50" name="ketentuan_satker" ></textarea>
                </div>
                <div class="form-group">
                    <span>Ketentuan Peserta Untuk Mengikuti Diklat Ini</span>
                    <label style="color: red;">*</label>
                    <textarea id="ket_peserta" class="form-control" rows="10" cols="50" name="ketentuan_peserta" ></textarea>
                </div>
                <div class="form-group">
                    <span>Dokumen yang diwajibkan untuk diupload pada persyaratan Diklat ini</span>
                    <label style="color: red;">*</label><br>
                    <table id="" class="table table-striped p-1" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col">Checkbox</th>
                                <th scope="col">Persyaratan Dokumen</th>
                            </tr>
                        </thead>        
                        <tbody>
                            <tr>
                                <?php $no = 1 ?>
                                @foreach($master_syarat_dokumen as $data)
                                <tr>
                                    <td><input type="checkbox" name="dokumen[]" value="<?=$data['id']?>"></td>
                                    <td><?=$data['nama']?></td>
                                </tr>
                                @endforeach
                            </tr>                              
                        </tbody>
                    </table>
                </div>
                 
                <a data-toggle="modal" data-target="#dokumenModal"  class="btn btn-primary">Tambah Persyaratan Lampiran (Jika Lampiran tidak ada pada checklist diatas)</a><br><br>
                            
                <div class="form-group">
                    <span>Status</span>
                    <label style="color: red;">*</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="status" required>
                        <option disable value="">===Pilih===</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>    
                <div class="modal-footer">            
                <div class="text-right">
                    <a href = "{{ url('/diklat') }}" type="button"class="btn btn-base btn-danger"><i class="fa fa-arrow-left ms-3"></i>  KEMBALI</a>
                </div> 
                <div class="text-left">
                    <button type="submit" class="btn btn-base btn-success">SIMPAN <i class="fa fa-folder ms-3"></i></button>
                </div> 
                </div>      
            </form>                 
        </div>
        </div>
    </div>
</div>
<!-- END CONTENT -->

<!-- MODAL -->
<div class="modal fade" id="dokumenModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="m-0 font-weight-bold text-primary">Tambah Persyaratan Dokumen</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>
            <form action="{{route('simpanpersyaratan')}}" method="POST">
                @csrf
                <div class="modal-body">
                     <span>Nama Dokumen</span>
                     <label style="color: red;">*</label>
                     <input type="text" class="form-control" id="validationCustom01"  name="nama_dokumen" >
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-base btn-success" >Simpan</button>
            </form>
        </div>
    </div>
</div>
<!-- END CONTENT -->

@endsection