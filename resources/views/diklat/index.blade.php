@extends('layout.admin')
@section('content')
<!-- CONTENT -->
<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h3 class="m-0 font-weight-bold text-primary">Master Diklat</h3>
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-download fa-sm text-white-50"></i> Unduh Data
        </a>
    </div>
    <div class="card mb-4">
        <div class="card-body">
        @include('sweetalert::alert')
            <div class="table-responsive">
				<table id="" class="table table-striped p-1" cellspacing="0" width="100%" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Jenis Diklat</th>
                            <th>Nama Diklat</th>
                            <th>Tanggal Sertifikat</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Akhir</th>
                            <th>Action</th>
                           
                        </tr>
                    </thead>          
                    <tbody>
                        <tr>
                            <?php $no = 1 ?>
                            @foreach($item as $diklat)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{$diklat['jenis_diklat']}}</td> 
                                <td>{{$diklat['nama_diklat']}}</td> 
                                <td>{{$diklat['tanggal_sertifikat']}}</td> 
                                <td>{{$diklat['tanggal_mulai']}}</td> 
                                <td>{{$diklat['tanggal_selesai']}}</td>  
                                <td>
                                    <a class="btn btn-success" href="{{ route('diklat.edit', $diklat['id']) }}">Simpan</a>
                                </td>                     
                            </tr>
                            @endforeach
                        </tr>                              
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT -->
<script>
   function myFunction() {
    var x = document.getElementById("Btn");
    x.disabled = true;
}
</script>
@endsection