@extends('layout.admin')
@section('content')
<!-- CONTENT -->
<div class="container-fluid">
    <div class="card mb-4">
        <div class="card-body">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h4 class="m-0 font-weight-bold text-primary">Detail Diklat</h4>
        </div>
        <form action="{{url('diklat/update')}}/{{$diklat->id}}" method="POST">
            @csrf
            @method('PATCH')
            <hr>
            <div class="form-group">
                <span>Judul Penyelenggaraan Diklat</span>
                <label style="color: red;">*</label>
                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="" name="judul" value="{{$diklat->judul}}" disabled>
            </div>
            <div class="form-group">
                <span>Jenis Diklat</span>
                <label style="color: red;">*</label>
                <select class="form-control" id="exampleFormControlSelect1" name="jenis_diklat" disabled>
                    <option disable value="">===Pilih===</option>
                    <option value="1" {{ $diklat->jenis_diklat == '1' ? 'selected' : '' }}>1</option>
                    <option value="2" {{ $diklat->jenis_diklat == '2' ? 'selected' : '' }}>2</option>
                    <option value="3" {{ $diklat->jenis_diklat == '3' ? 'selected' : '' }}>3</option>
                    <option value="4" {{ $diklat->jenis_diklat == '4' ? 'selected' : '' }}>4</option>
                </select>
            </div>
            <div class="form-group">
                <span>Kategori Diklat</span>
                <label style="color: red;">*</label>
                <select class="form-control" id="exampleFormControlSelect1" name="kategori_diklat"  disabled>
                    <option disable value="">===Pilih===</option>
                    <option value="1" {{ $diklat->kategori_diklat == '1' ? 'selected' : '' }}>1</option>
                    <option value="2" {{ $diklat->kategori_diklat == '2' ? 'selected' : '' }}>2</option>
                    <option value="3" {{ $diklat->kategori_diklat == '3' ? 'selected' : '' }}>3</option>
                    <option value="4" {{ $diklat->kategori_diklat == '4' ? 'selected' : '' }}>4</option>
                </select>
            </div>
            <div class="form-group">
                <span>Nama lembaga yang menyelenggarakan</span>
                <label style="color: red;">*</label>
                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="" name="nama_lembaga" value="{{$diklat->nama_lembaga}}" disabled>
            </div>
            <div class="form-group">
                <span>Tempat Diklat</span>
                <label style="color: red;">*</label>
                <select class="form-control" id="exampleFormControlSelect1" name="tempat_diklat" disabled>
                    <option disable value="">===Pilih===</option>
                    <option value="1" {{ $diklat->tempat == '1' ? 'selected' : '' }}>1</option>
                    <option value="2" {{ $diklat->tempat == '2' ? 'selected' : '' }}>2</option>
                    <option value="3" {{ $diklat->tempat == '3' ? 'selected' : '' }}>3</option>
                    <option value="4" {{ $diklat->tempat == '4' ? 'selected' : '' }}>4</option>
                 </select>
            </div>
            <div  class="row g-2 needs-validation">
                <div class="col-md-6">
                    <span>Tanggal Mulai Pendaftaran Diklat</span>
                    <label style="color: red;">*</label>
                    <input type="date" class="form-control" id="validationCustom01"  name="tanggal_mulaidaftar" value="{{$diklat->tanggal_mulaidaftar}}" disabled>
                </div>  
                <div class="col-md-6">
                    <span>Tanggal Akhir Pendaftaran Diklat</span>
                    <label style="color: red;">*</label>
                    <input type="date" class="form-control" id="validationCustom01"  name="tanggal_akhirdaftar" value="{{$diklat->tanggal_akhirdaftar}}" disabled>
                </div>
            </div><br>
            <div  class="row g-2 needs-validation">
                <div class="col-md-6">
                    <span>Tanggal Mulai Diklat</span>
                    <label style="color: red;">*</label>
                    <input type="date" class="form-control" id="validationCustom01"  name="tanggal_mulai" value="{{$diklat->tanggal_mulai}}" disabled>
                </div>
                <div class="col-md-6">
                    <span>Tanggal Selesai Diklat</span>
                    <label style="color: red;">*</label>
                    <input type="date" class="form-control" id="validationCustom01"  name="tanggal_selesai" value="{{$diklat->tanggal_selesai}}" disabled>
                </div>
            </div><br>
            <div class="form-group">
                <span>Jumlah Peserta (Orang)</span>
                <label style="color: red;">*</label>
                <input type="number" class="form-control" id="exampleFormControlInput1" placeholder="" name="jumlah_peserta" value="{{$diklat->jumlah_peserta}}" disabled>
            </div>
            <div  class="row g-2 needs-validation">
                <div class="col-md-6">
                    <span>Durasi</span>
                    <label style="color: red;">*</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="" name="durasi" value="{{$diklat->durasi}}"  disabled>
                </div>
                <div class="col-md-6">
                    <span>Satuan Durasi</span>
                    <label style="color: red;">*</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="satuan_durasi" disabled>
                        <option value="menit" {{ $diklat->satuan_durasi == 'menit' ? 'selected' : '' }}>Menit</option>
                        <option value="jam" {{ $diklat->satuan_durasi== 'jam' ? 'selected' : '' }}>Jam</option>
                        <option value="detik" {{ $diklat->satuan_durasi== 'detik' ? 'selected' : '' }}>detik</option>
                    </select>
                </div>
            </div><br>
            <div class="form-group">
                <span>Ketentuan Umum Mengenai Diklat Ini</span>
                <label style="color: red;">*</label>
                <textarea id="ket_umum_detail" class="form-control" rows="10" cols="50" name="ketentuan_umum" disabled="disabled">{{$diklat->ketentuan_umum}}</textarea>
            </div>
            <div class="form-group">
                <span>Satuan Kerja Yang Ditunjuk Untuk Mengikuti Diklat Ini</span>
                <label style="color: red;">*</label>
                <textarea id="ket_satker_detail" class="form-control" rows="10" cols="50" name="ketentuan_satker" disabled="disabled">{{$diklat->ketentuan_satker}}</textarea>
            </div>
            <div class="form-group">
                <span>Ketentuan Peserta Untuk Mengikuti Diklat Ini</span>
                <label style="color: red;">*</label>
                <textarea id="ket_peserta_detail" class="form-control" rows="10" cols="50" name="ketentuan_peserta" disabled="disabled">{{$diklat->ketentuan_peserta}}</textarea>
            </div>
            <div class="form-group">
                <span>Dokumen yang diwajibkan untuk diupload pada persyaratan Diklat ini</span>
                <label style="color: red;">*</label><br>
                    <?php
                    $dokumen = json_decode($diklat->dokumen);
                    //    print_r($dokumen)
                    // echo $dokumen[0];
                    ?>
                    <table id="" class="table table-striped p-1" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col">Checkbox</th>
                                <th scope="col">Persyaratan Dokumen</th>
                            </tr>
                        </thead>    
                        <tbody>
                            <tr>
                                <?php $no = 1 ?>
                                @foreach($master_syarat_dokumen as $data)
                                <tr>
                                    <td><input type="checkbox" name="dokumen[]" value="<?=$data['id']?>" {{ in_array($data['id'], $dokumen) ? 'checked':''}} disabled></td>
                                    <td><?=$data['nama']?></td>
                                </tr>
                                @endforeach
                            </tr>                              
                        </tbody>
                    </table>
            </div>
            <div class="form-group">
                <span>Status</span>
                <label style="color: red;">*</label>
                <select class="form-control" id="exampleFormControlSelect1" name="status" disabled>
                    <option disable value="">===Pilih===</option>
                    <option value="1" {{ $diklat->status == '1' ? 'selected' : '' }}>1</option>
                    <option value="2" {{ $diklat->status == '2' ? 'selected' : '' }}>2</option>
                    <option value="3" {{ $diklat->status == '3' ? 'selected' : '' }}>3</option>
                    <option value="4" {{ $diklat->status == '4' ? 'selected' : '' }}>4</option>
                    <option value="5" {{ $diklat->status == '5' ? 'selected' : '' }}>5</option>
                </select>
            </div>                 
        </form>
        <div class="modal-footer">   
                <div class="pull-right">
                    <a href = "{{ url('/diklat') }}" type="button" id="loads" class="btn btn-base btn-danger" style=" "><i class="fa fa-arrow-left me-3"></i>  KEMBALI</a>
                </div>           
        </div>           
    </div>
</div>
<!-- END CONTENT -->

<script src="https://cdn.ckeditor.com/ckeditor5/36.0.1/classic/ckeditor.js"></script>
<script>
        ClassicEditor
            .create( document.querySelector( '#ket_umum_detail' ) )
            .attr("disabled",true);
            .then( ket_umum_detail => {
                                console.log( ket_umum_detail );
                                config.readOnly = true;
                             } )
                                .catch( error => {
                                console.error( error );
                             } );   
        ClassicEditor
            .create( document.querySelector( '#ket_satker_detail' ) )
            .attr("disabled",true);
            .then( ket_satker_detail => {
                                console.log( ket_satker_detail );
                             } )
                                .catch( error => {
                                console.error( error );
                             } ); 
        ClassicEditor
            .create( document.querySelector( '#ket_peserta_detail' ) )
            .attr("disabled",true);
            .then( ket_peserta_detail => {
                                console.log( ket_peserta_detail );
                             } )
                                .catch( error => {
                                console.error( error );
                             } );                 
</script>
@endsection