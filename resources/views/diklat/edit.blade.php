@extends('layout.admin')
@section('content')
<!-- CONTENT -->
<div class="container-fluid">
    <div class="card mb-4">
        <div class="card-body">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h4 class="m-0 font-weight-bold text-primary">Form Tambah Diklat</h4>
            </div>
            <form action="{{url('diklat/update')}}/{{$item['id']}}" method="POST">
                @csrf
                @method('PATCH')
                <hr>
                <div class="form-group">
                    <span>id</span>
                    <label style="color: red;">*</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="" name="id" value="{{$item['id']}}">
                </div>
              
                <div class="modal-footer">   
                <div class="pull-right">
                    <a href = "{{ url('/diklat') }}" type="button" id="loads" class="btn btn-base btn-danger" style=" "><i class="fa fa-arrow-left me-3"></i>  KEMBALI</a>
                </div>    
                <div class="pull-left">
                    <button type="submit" class="btn btn-base btn-success">SIMPAN <i class="fa fa-folder ms-3"></i></button>
                </div>    
                </div>    
            </form>                  
        </div>
    </div>
</div>
<!-- END CONTENT -->


@endsection