@extends('layout.admin')
@section('content')
<div class="container-fluid">
    <div class="card mb-4">
        <div class="card-body">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h4 class="m-0 font-weight-bold text-primary">Tambah Pertanyaan Diklat</h4>
            </div>
            <form action="{{route('master_pertanyaan.store')}}" method="POST">
                @csrf
                <div class="form-group">
                     <span>Pertanyaan</span>
                     <label style="color: red;">*</label>
                     <input type="text" class="form-control" id="validationCustom01"  name="pertanyaan" placeholder="Masukkan Pertanyaan...">
                </div>
                <div class="form-group">
                        <span>Tipe Pertanyaan</span>
                        <label style="color: red;">*</label>
                        <select name="jenis_pertanyaan"  onchange="tujuan(this);" id="status" class="form-control select2" required>
                        <option value="pilihan">Pilih Tipe Pelayanan</option>
                        <option value="">Pilihan Ganda</option>
                        <option value="1">Essai</option>
                        </select>
                </div>
                <div  class="form-group" id="view_jawaban" style="display: none;">
                    <div class="form-group">
                        <span>Pertanyaan A</span>
                        <input type="text" class="form-control" id="validationCustom01"  name="jawaban_a">
                    </div>
                    <div class="form-group">
                        <span>pertanyaan B</span>
                        <input type="text" class="form-control" id="validationCustom01"  name="jawaban_b">
                    </div>
                    <div class="form-group">
                        <span>pertanyaan C</span>
                        <input type="text" class="form-control" id="validationCustom01"  name="jawaban_c">
                    </div>
                    <div class="form-group">
                        <span>pertanyaan D</span>
                        <input type="text" class="form-control" id="validationCustom01"  name="jawaban_d">
                    </div>
                    <div class="form-group">
                        <span>pertanyaan E</span>
                        <input type="text" class="form-control" id="validationCustom01"  name="jawaban_e">
                    </div> 
                </div>
                <div class="modal-footer">
                <a href = "{{ url('/master_pertanyaan') }}" type="button" id="loads" class="btn btn-base btn-danger" style=" "><i class="fa fa-arrow-left me-3"></i>  KEMBALI</a>
                <button type="submit" class="btn btn-base btn-success">SIMPAN <i class="fa fa-folder ms-3"></i></button>
                </div>
            </form>
        </div>
        </div>
    </div>                 
</div>

<script type="text/javascript">
    function tujuan(that) {
        if (that.value == "") {
            document.getElementById("view_jawaban").style.display = "block";
        } else if (that.value == "1") {  
            document.getElementById("view_jawaban").style.display = "none";
        } 
    }
</script>
@endsection