@extends('layout.admin')
@section('content')
<div class="container-fluid">
    <div  class="card mb-4">
        <div class="card-body">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h4 class="m-0 font-weight-bold text-primary">Edit Pertanyaan Diklat</h4>
            </div>
            <form action="{{route('master_pertanyaan.update', $master_pertanyaan->id)}}" method="POST">
                @csrf
                @method('PATCH')
                <hr>
                <div class="form-group">
                    <span>id</span>
                    <input type="text" class="form-control" id="validationCustom01"  name="pertanyaan" value="{{$master_pertanyaan->pertanyaan}}">
                </div>
                <div class="form-group">
                    <span>Tipe Pelayanan</span>
                    <label style="color: red;">*</label>
                    <select name="jenis_pertanyaan"  onchange="tujuan(this);" id="status" class="form-control select2" required>
                        <option value="pilihan">Pilih Tipe Pelayanan</option>
                        <option value="" {{ $master_pertanyaan->is_essai == '' ? 'selected' : '' }}>Pilihan Ganda</option>
                        <option value="1" {{ $master_pertanyaan->is_essai == '1' ? 'selected' : '' }}>Essai</option>
                    </select>
                </div>
                <div class="card card-body" id="view_essai" style="display: none;" type="hidden">
                    <div class="form-group">
                        <span>Pertanyaan A</span>
                        <input  type="text" class="form-control" id="validationCustom01" name="jawaban_a" value="{{$master_pertanyaan->jawaban_a}}">
                    </div>
                    <div class="form-group">
                        <span>pertanyaan B</span>
                        <input type="text" class="form-control" id="validationCustom01" name="jawaban_b" value="{{$master_pertanyaan->jawaban_b}}">
                    </div>
                    <div class="form-group">
                        <span>pertanyaan C</span>
                        <input type="text" class="form-control" id="validationCustom01" name="jawaban_c" value="{{$master_pertanyaan->jawaban_c}}">
                    </div>
                    <div class="form-group">
                        <span>pertanyaan D</span>
                        <input type="text" class="form-control" id="validationCustom01" name="jawaban_d" value="{{$master_pertanyaan->jawaban_d}}">
                    </div>
                    <div class="form-group">
                        <span>pertanyaan E</span>
                        <input type="text" class="form-control" id="validationCustom01" name="jawaban_e" value="{{$master_pertanyaan->jawaban_e}}">
                    </div> 
                </div>
                <div class="card card-body"  name="jenis" id="view_jawaban" style="display: none;">
                    <div class="form-group">
                        <span>Pertanyaan A</span>
                        <input type="text" class="form-control" id="validationCustom01" name="jawaban_a" value="{{$master_pertanyaan->jawaban_a}}">
                    </div>
                    <div class="form-group">
                        <span>pertanyaan B</span>
                        <input type="text" class="form-control" id="validationCustom01" name="jawaban_b" value="{{$master_pertanyaan->jawaban_b}}">
                    </div>
                    <div class="form-group">
                        <span>pertanyaan C</span>
                        <input type="text" class="form-control" id="validationCustom01" name="jawaban_c" value="{{$master_pertanyaan->jawaban_c}}">
                    </div>
                    <div class="form-group">
                        <span>pertanyaan D</span>
                        <input type="text" class="form-control" id="validationCustom01" name="jawaban_d" value="{{$master_pertanyaan->jawaban_d}}">
                    </div>
                    <div class="form-group">
                        <span>pertanyaan E</span>
                        <input type="text" class="form-control" id="validationCustom01" name="jawaban_e" value="{{$master_pertanyaan->jawaban_e}}">
                    </div> 
                </div>
                <div class="modal-footer"> 
                    <a href = "{{ url('/master_pertanyaanindex') }}" type="button" id="loads" class="btn btn-base btn-danger" style=" "><i class="fa fa-arrow-left me-3"></i>  KEMBALI</a>
                    <button type="submit" class="btn btn-base btn-success">SIMPAN <i class="fa fa-folder ms-3"></i></button>
                </div>    
            </form>
                   
        </div>
    </div>
</div>

<script type="text/javascript">
   document.getElementById("status").dispatchEvent(new Event('change'));
    // ini tuh buat nge trigger / onload, karena ini gk pake jquery ya ? jadinya kita gk bisa pake $('id atau apalah')
    // makanya ini gua bikin pake vanilla js ya, fungsi dispatchevent itu buat ngetrigger event yg dipunya, kaya onchange, onload, onclick sesuai sama property / param didalem Event('ini param');
    // share sama putri ya
    // titip ganti button dropdown ditable nya wkwk, itu jadi nambah 2 step cuman buat pilih opsi, mestinya 1 klik ajah cukup jadi kasih 3 pilihan langsung tampil
    document.getElementById("status").dispatchEvent(new Event('change'));
    function tujuan(that) {
        if (that.value == "") {
            document.getElementById("view_jawaban").style.display = "block";
        } else if (that.value == "1") {       
            document.getElementById("view_jawaban").style.display = "none";
            document.getElementById("view_essai").style.display = "blcok";
        }
    }
</script>
@endsection