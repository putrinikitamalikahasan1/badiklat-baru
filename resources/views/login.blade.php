<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login Badiklat</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"></head>

<body style="background-image:url({{ asset('assets/img/gambar.png') }})">
<div class="container">
<div class="row">
    <div class="col-sm-7">
    </div>
    <div class="col-md-5" style="margin-top:3rem;">
        <div class="card mb-4 rounded shadow-sm p-3 bg-white mx-auto">
            <div class="card-body">
            @include('sweetalert::alert')
            <form action="{{ route('actionlogin') }}" method="post">
                    @csrf
                    <center>
                    <img class="text-center mx-auto" src="{{asset('assets/img/logo-kejaksaan.png')}}" style="width: 100px" />
                        <h4>Badiklat</h4>
                    <span>Badan Pendidikan dan Pelatihan Kemhan RI</span>
                     </center>
                     <hr> 
                     @if(session('error'))
                    <div class="alert alert-danger">
                        <b>Opps!</b> {{session('error')}}
                    </div>
                    @endif
                    <div class="form-group mt-3">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control" placeholder="Email" required="">
                    </div>

                    <div class="form-group mt-3">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Password" required="">
                    </div></br>

                    <div class="text-center">
                    <button type="submit" class="btn btn-primary btn-block">Log In</button>
                    </div>
                    <hr>
                    <center><span class="text-secondary">© 2023 Badiklat Kejaksaan RI. All Rights Reserved.</span></center>
                </form>
            </div>
        </div>
    </div>
          
</div>
</div>
</body>
</html>
                      